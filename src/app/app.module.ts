import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { MyresultsComponent } from './myresults/myresults.component';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ResultsComponent } from './results/results.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeModule } from './home/home.module';
import { LoginComponent } from './login/login.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { JwPaginationComponent } from 'jw-angular-pagination';

import { MagazineComponent } from './magazine/magazine.component';
import { FicheComponent } from './fiche/fiche.component';
// import {PlayerficheComponent } from './playerfiche/playerfiche.component';
import { MagazineconseilsComponent } from './magazineconseils/magazineconseils.component';
import { ContactComponent } from './contact/contact.component';
import { PlayerficheinfoComponent } from './playerficheinfo/playerficheinfo.component';
import { PolitiquesconfidentialiteComponent } from './politiquesconfidentialite/politiquesconfidentialite.component';

import { DashboardCoachComponent } from './dashboardCoach/dashboardCoach.component';
import { ModifierfichecoachComponent } from './modifierfichecoach/modifierfichecoach.component';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LandingComponent,
    MyresultsComponent,
    ProfileComponent,
    NavbarComponent,
    DashboardComponent,
    ResultsComponent,
    MagazineComponent,
    ContactComponent,
    PolitiquesconfidentialiteComponent,
    MagazineconseilsComponent,
    FicheComponent,
    PlayerficheinfoComponent,
    DashboardCoachComponent,
    ModifierfichecoachComponent, 
    FooterComponent,
    JwPaginationComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HomeModule,
    HttpClientModule,
       TranslateModule.forRoot({
           loader: {
               provide: TranslateLoader,
               useFactory: HttpLoaderFactory,
               deps: [HttpClient]
           }
       })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
