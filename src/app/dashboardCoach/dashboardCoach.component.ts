import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
@Component({
selector: 'app-dashboardCoach',
templateUrl: './dashboardCoach.component.html',
styleUrls: ['./dashboardCoach.component.scss']
})

export class DashboardCoachComponent implements OnInit {


private activepage = 'dashboard';
private dashactive = 'top-menu--active';
private chatactive = '';
private rechercheactive = '';
private favorisactive = '';
private ficheactive = '';
private modifierficheactive = '';
constructor(
@Inject(DOCUMENT) private document: Document
) {


}

ngOnInit() {
this.document.body.classList.add ('app');

}


setPage(page){
if(page == 'chat'){
this.dashactive = '';
this.chatactive = 'top-menu--active';
this.ficheactive = '';
this.favorisactive = '';
this.rechercheactive = '';
this.modifierficheactive = '';

}
else if(page == 'dashboard'){
this.dashactive = 'top-menu--active';
this.chatactive = '';
this.ficheactive = '';
this.rechercheactive = '';
this.favorisactive = '';
this.modifierficheactive = '';

}
else if(page == 'favoris'){
this.dashactive = '';
this.chatactive = '';
this.ficheactive = '';
this.rechercheactive = '';
this.favorisactive = 'top-menu--active';
this.modifierficheactive = '';

}
else if(page == 'recherche'){
this.dashactive = '';
this.chatactive = '';
this.ficheactive = '';
this.rechercheactive = 'top-menu--active';
this.favorisactive = '';
this.modifierficheactive = '';

}
else if(page == 'fiche'){
this.dashactive = '';
this.chatactive = '';
this.ficheactive = 'top-menu--active';
this.rechercheactive = '';
this.favorisactive = '';
this.modifierficheactive = '';

}
else if(page == 'modifier-fiche-coach'){
this.dashactive = '';
this.chatactive = '';
this.ficheactive = '';
this.rechercheactive = '';
this.favorisactive = '';
this.modifierficheactive = 'top-menu--active';

}
else{
this.dashactive = '';
this.chatactive = '';
this.chatactive = '';
this.ficheactive = '';
this.ficheactive = '';
this.modifierficheactive = '';

}
this.activepage = page;
}
}
