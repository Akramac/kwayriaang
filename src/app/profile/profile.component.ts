import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
   private activepage = 'profile';
   private fichemenu =false;
    constructor() { }


    ngOnInit() {}
    setPage(page){
      this.activepage = page   ;

      if(page == 'fiche' || page =='photo' || page =='video'){
       this.fichemenu = true;

      }else{

         this.fichemenu = false;
      }
    }
}
