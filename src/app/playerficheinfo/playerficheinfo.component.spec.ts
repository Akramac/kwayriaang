import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerficheinfoComponent } from './playerficheinfo.component';

describe('PlayerficheinfoComponent', () => {
  let component: PlayerficheinfoComponent;
  let fixture: ComponentFixture<PlayerficheinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerficheinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerficheinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
