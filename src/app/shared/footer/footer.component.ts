import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Location, PopStateEvent } from '@angular/common';


@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    test : Date = new Date();
    public isCollapsed = true;
    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];
    private toshow = true;
    private timer = false;
    private showInRegistre = false;

    constructor(public location: Location, private router: Router ) {}

    ngOnInit() {

     var title = this.location.prepareExternalUrl(this.location.path());
     if(title === '#/login' || title === "#/register" ){
       this.toshow = false;
       this.showInRegistre = true;
     }else{

        this.toshow = true;
     }

     if(title === "#/examens"){
       this.timer = true;
     }
    }
    getPath(){
      return this.router.url;
    }
}
