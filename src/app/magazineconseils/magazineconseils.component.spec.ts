import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagazineconseilsComponent } from './contact.component';

describe('MagazineconseilsComponent', () => {
  let component: MagazineconseilsComponent;
  let fixture: ComponentFixture<MagazineconseilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MagazineconseilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagazineconseilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
