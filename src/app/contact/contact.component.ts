import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import { HttpClient } from "@angular/common/http";
import { DOCUMENT } from '@angular/common';
import {  Inject, OnDestroy, Renderer2 } from '@angular/core';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {
  private toshow = true;
  private data = "tata";
  categories: any = [];
    model = {
        left: true,
        middle: false,
        right: false
    };

    focus;
    focus1;
    constructor(
    private router: Router,
    private translate: TranslateService,
    @Inject(DOCUMENT) private document: Document,
    private httpClient: HttpClient
  ) {
translate.setDefaultLang('fr');
   }

    ngOnInit() {
      this.document.body.classList.add ('public-layout');
      this.httpClient.get("/assets/data.json").subscribe(data =>{
           console.log(data);
           this.categories = data;
         })
    }

    goToLogin(){

      this.router.navigate(['/login']);

    }
}
