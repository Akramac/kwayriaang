import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ResultsComponent } from './results/results.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { MyresultsComponent } from './myresults/myresults.component';
import { LoginComponent } from './login/login.component';

import { MagazineComponent } from './magazine/magazine.component';
import { MagazineconseilsComponent } from './magazineconseils/magazineconseils.component';
import { ContactComponent } from './contact/contact.component';
import { PolitiquesconfidentialiteComponent } from './politiquesconfidentialite/politiquesconfidentialite.component';
import { FicheComponent } from './fiche/fiche.component';
import { PlayerficheinfoComponent } from './playerficheinfo/playerficheinfo.component';

//

import { ModifierfichecoachComponent } from './modifierfichecoach/modifierfichecoach.component';

import { DashboardCoachComponent } from './dashboardCoach/dashboardCoach.component';

const routes: Routes =[
{ path: 'home',             component: HomeComponent },
{ path: 'modifier-fiche-coach',             component: ModifierfichecoachComponent },
{ path: 'playerficheinfo',             component: PlayerficheinfoComponent },
{ path: 'fiche',             component: FicheComponent },
{ path: 'magazine-conseils',             component: MagazineconseilsComponent },
{ path: 'politiques-confidentialite',             component: PolitiquesconfidentialiteComponent },
{ path: 'contact',             component: ContactComponent },
{ path: 'magazine',             component: MagazineComponent },
{ path: 'profile',     component: ProfileComponent },
{ path: 'register',          component: SignupComponent },
{ path: 'exams',      component: LandingComponent },
{ path: 'results',          component: ResultsComponent },
{ path: 'dashboard',          component: DashboardComponent },
{ path: 'dashboard-coach',          component: DashboardCoachComponent },
{ path: 'resultat-search',  component: MyresultsComponent },
{ path: 'login',            component: LoginComponent },
{ path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
imports: [
CommonModule,
BrowserModule,
RouterModule.forRoot(routes,{
useHash: true
})
],
exports: [
],
})
export class AppRoutingModule { }
