import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    selector: 'app-Results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss']
})

export class ResultsComponent implements OnInit {
  private toshow = true;
  private data = "tata";
    model = {
        left: true,
        middle: false,
        right: false
    };

    focus;
    focus1;
    constructor(
    private router: Router
  ) { }

    ngOnInit() {

    }

    goToExam(){
      this.router.navigate(['/resultat-examen']);

    }
}
