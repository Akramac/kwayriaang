import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
   private activepage = 'dashboard';
   private dashactive = 'top-menu--active';
   private chatactive = '';
   private ficheactive = '';
    constructor(
       @Inject(DOCUMENT) private document: Document
    ) { }

    ngOnInit() {
 this.document.body.classList.add ('app');

    }
    setPage(page){
      if(page == 'chat'){
        this.dashactive = '';
        this.chatactive = 'top-menu--active';
        this.ficheactive = '';
      }
      else if(page == 'dashboard'){
        this.dashactive = 'top-menu--active';
        this.chatactive = '';
        this.ficheactive = '';
      }else{
        this.dashactive = '';
        this.chatactive = '';
        this.ficheactive = 'top-menu--active';

      }
      this.activepage = page;
    }
}
