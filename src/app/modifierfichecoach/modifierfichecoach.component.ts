import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
@Component({
    selector: 'app-modifierfichecoach',
    templateUrl: './modifierfichecoach.component.html',
    styleUrls: ['./modifierfichecoach.component.scss']
})

export class ModifierfichecoachComponent implements OnInit {
  private activepage = 'dashboard';
  private dashactive = 'top-menu--active';
  private chatactive = '';
  private rechercheactive = '';
  private favorisactive = '';
  private ficheactive = '';
   constructor(
      @Inject(DOCUMENT) private document: Document
   ) { }

   ngOnInit() {
 this.document.body.classList.add ('app');

   }
   setPage(page){
     if(page == 'chat'){
       this.dashactive = '';
       this.chatactive = 'top-menu--active';
       this.ficheactive = '';
       this.favorisactive = '';
       this.rechercheactive = '';

     }
     else if(page == 'dashboard'){
       this.dashactive = 'top-menu--active';
       this.chatactive = '';
       this.ficheactive = '';
       this.rechercheactive = '';
       this.favorisactive = '';

     }
     else if(page == 'favoris'){
       this.dashactive = '';
       this.chatactive = '';
       this.ficheactive = '';
       this.rechercheactive = '';
       this.favorisactive = 'top-menu--active';

     }
     else if(page == 'recherche'){
       this.dashactive = '';
       this.chatactive = '';
       this.ficheactive = '';
       this.rechercheactive = 'top-menu--active';
       this.favorisactive = '';

     }
     else{
       this.dashactive = '';
       this.chatactive = '';
       this.chatactive = '';
       this.ficheactive = '';
       this.ficheactive = 'top-menu--active';

     }
     this.activepage = page;
   }
}
