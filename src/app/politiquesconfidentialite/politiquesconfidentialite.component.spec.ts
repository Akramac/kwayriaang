import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolitiquesconfidentialiteComponent } from './politiquesconfidentialite.component';

describe('PolitiquesconfidentialiteComponent', () => {
  let component: PolitiquesconfidentialiteComponent;
  let fixture: ComponentFixture<PolitiquesconfidentialiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolitiquesconfidentialiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolitiquesconfidentialiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
