$(document).ready(function(){

    $(document).on('click',".dev-filter-drop",function(e) {
        $('.dev-filter-drop').each(function(){
            $(this).removeClass('open');
        });
        $(this).addClass("open");
    });
    $('.dev-filter-drop').each(function(){
        var elm       = $(this);
        var id        = elm.attr('id');
        var btnClose  = elm.find('.drop-close');
        var dropDetail = elm.find('.drop-detail');
        var btnCancel  = elm.find('.btn-cancel');
        
        btnCancel.on('click',function() {
            elm.removeClass('open');
        });
        
        if(id == "city-filter")
        {
            var radio = elm.find(':input');
            var formInput = $('input[name="city"]');

            radio.on('change',function(){
                if($(this).is(':checked'))
                {
                    formInput.val($(this).val());
                    dropDetail.html($(this).val());
                    elm.addClass('active');
                }
            });
            
            btnClose.on('click',function(e){
                e.preventDefault();
                formInput.val('');
                dropDetail.html('');
                radio.prop("checked", false);
                elm.removeClass('active');
            });
        }
        else if(id == "age-filter")
        {
            var btnValidate  = elm.find('.btn-validate');
            var inputMin     = $('#min-age');
            var inputMax     = $('#max-age');
            var formInputMin = $('input[name="min_age"]');
            var formInputMax = $('input[name="max_age"]');
            
            btnValidate.on('click',function() {
                formInputMin.val(inputMin.val());
                formInputMax.val(inputMax.val());
                dropDetail.html(inputMin.val()+' - '+inputMax.val());
                elm.addClass('active');
                elm.removeClass('open');
            });
            
            btnClose.on('click',function(e){
                e.preventDefault();
                formInputMin.val('');
                formInputMax.val('');
                dropDetail.html('');
                elm.removeClass('active');
            });
        }
        
        else if(id == "height-filter")
        {
            var btnValidate  = elm.find('.btn-validate');
            var inputMin     = $('#min-height');
            var inputMax     = $('#max-height');
            var formInputMin = $('input[name="min_height"]');
            var formInputMax = $('input[name="max_height"]');
            
            btnValidate.on('click',function() {
                formInputMin.val(inputMin.val());
                formInputMax.val(inputMax.val());
                dropDetail.html(inputMin.val()+' - '+inputMax.val());
                elm.addClass('active');
                elm.removeClass('open');
            });
            
            btnClose.on('click',function(e){
                e.preventDefault();
                formInputMin.val('');
                formInputMax.val('');
                dropDetail.html('');
                elm.removeClass('active');
            });
        }
        else if(id == "weight-filter")
        {
            var btnValidate  = elm.find('.btn-validate');
            var inputMin     = $('#min-weight');
            var inputMax     = $('#max-weight');
            var formInputMin = $('input[name="min_weight"]');
            var formInputMax = $('input[name="max_weight"]');
            
            btnValidate.on('click',function() {
                formInputMin.val(inputMin.val());
                formInputMax.val(inputMax.val());
                dropDetail.html(inputMin.val()+' - '+inputMax.val());
                elm.addClass('active');
                elm.removeClass('open');
            });
            
            btnClose.on('click',function(e){
                e.preventDefault();
                formInputMin.val('');
                formInputMax.val('');
                dropDetail.html('');
                elm.removeClass('active');
            });
        }
        else if(id == "position-filter")
        {
            var radio = elm.find(':input');
            var formInput = $('input[name="position"]');
            
            radio.on('change',function(){
                if($(this).is(':checked'))
                {
                    formInput.val($(this).val());
                    dropDetail.html($(this).val());
                    elm.addClass('active');
                }
            });
            
            btnClose.on('click',function(e){
                e.preventDefault();
                formInput.val('');
                dropDetail.html('');
                radio.prop("checked", false);
                elm.removeClass('active');
            });
        }
        else if(id == "foot-filter")
        {
            var radio = elm.find(':input');
            var formInput = $('input[name="dominant_foot"]');
            
            radio.on('change',function(){
                if($(this).is(':checked'))
                {
                    formInput.val($(this).val());
                    dropDetail.html($(this).val());
                    elm.addClass('active');
                }
            });
            
            btnClose.on('click',function(e){
                e.preventDefault();
                formInput.val('');
                dropDetail.html('');
                radio.prop("checked", false);
                elm.removeClass('active');
            });
        }
        
        
    });

$('#btn-apply').click(function() {
 $('#filters-form').submit();
});

});