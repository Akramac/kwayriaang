$(document).ready(function(){
    $("#register-form").on('submit',function(e) { e.preventDefault(); });
    $(".next-step").click(function(e){
        e.preventDefault();
        var btn = $(this);
        var parent =  $(this).parents('.inscription-step');
        var next = parent.next('.inscription-step:first');
        
        
        
        parent.addClass('hidden');
        next.removeClass('hidden');
      
        
   });
   $(".prev-step").click(function(e){
        e.preventDefault();
        var btn = $(this);
        var parent =  $(this).parents('.inscription-step');
        var prev = parent.prev('.inscription-step:first');
        parent.addClass('hidden');
        prev.removeClass('hidden');
        
   });
   
   $("#send-register-form").click(function(e){
      e.preventDefault();
      
      var form    = $("#register-form");
      var url     = $("#register-form").attr('action');
      
      $('.is-invalid').removeClass('is-invalid');
      
      $.ajax({
				type : 'POST',
				url : url,
				data : form.serialize(),
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			})
		.done((data) => {
			
			if(data.done)
				window.location.href = data.redirect_url;
			
			
        })
        .fail((data) => {
            if(data.status == 422) {
                var response = JSON.parse(data.responseText);
                $.each(response.errors, function (i, error) {
                    $('#register-form')
                        .find('[name="' + i + '"]')
                        .addClass('is-invalid');
                });
            }
        });
      
      
   });
   
});
   