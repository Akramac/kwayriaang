$(document).ready(function() {
    $(".temo-modal .slick-prev").css("display","none");
    $(".temo-modal .slick-next").css("display","none");
    $(".temo-modal .slick-dots li").css("background-color", "#222a51;");
    $(".temo-modal .slick-dots li.slick-active button:before").css("background-color", "#6bd32c");
    //After changing video type
    $("#video-types").change(function() {
        var selectedType = $(this).children("option:selected").val();
        if (selectedType == "local") {
            $("#video-url").attr({"type":"file","placeholder":""});
            $("#apercu-block").css("display", "none");

        } else {
            $("#video-url").attr({"type":"text","placeholder":"Coller le lien"});
            $("#apercu-block").css({"display":"flex",'background-image':'none'});
            $("#abs-apercu").css("display", "inline");
        }
    });
    //Deleting videos
    $(".delete-video").click(function() {
        if (confirm("voulez-vous supprimer ce video?")) {
            var id = $(this).attr('id');
            $.ajax({
                type: "POST",
                url: '/player/fiche/videos/deleteForm',
                data: {
                    id: id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                success: function(response) {
                    $("#cont-" + id).css('display', 'none');
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("some error");
                }
            });
        }
        return false;
    });
    //Deleting images
    $(".delete-image").click(function() {
        if (confirm("voulez-vous supprimer cette image?")) {
            var id = $(this).attr('id');
            $.ajax({
                type: "POST",
                url: '/player/fiche/images/deleteForm',
                data: {
                    id: id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                success: function(response) {
                    $("#cont-" + id).css('display', 'none');
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("some error");
                }
            });
        }
        return false;
    });
    //uploading page
    $('#but-rel').click(function() {
        location.reload();
    });
});
//controle the apercu block
function apercu() {
    $("#apercu-block").css({"display":"flex",'background-image':'none'});
    $("#abs-apercu").css("display", "inline");
    var type = $("#video-url").attr("type");
    var value = $("#video-url").val();
    if (type == "text") {
        var re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
        video_id = re.exec($("#video-url").val())[1];
        var img = "https://img.youtube.com/vi/" + video_id + "/maxresdefault.jpg";
        $("#apercu-block").css('background-image', 'url(' + img + ')');
        $("#abs-apercu").css("display", "none");
    } else {
        $("#apercu-block").css("display", "none");
    }
}